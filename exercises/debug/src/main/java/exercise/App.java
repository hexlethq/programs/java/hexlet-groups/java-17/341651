package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        String result = null;
        if ((a + b <= c) || (a + c <= b) || (b + c <= a)) {
            result = "Треугольник не существует";
        } else if ((a != b) && (a != c) && (b != c)) {
            result = "Разносторонний";
        } else if ((a == b) && (b == c)) {
            result = "Равносторонний";
        } else if (a == b || a == c || b == c) {
            result = "Равнобедренный";
        }
        return result;
    }
    // END
}
