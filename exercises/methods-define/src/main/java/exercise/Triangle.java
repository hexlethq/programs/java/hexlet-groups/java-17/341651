package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double a, double b, double degrees) {
        double result = a * b * 0.5 * Math.sin(Math.toRadians(degrees));
        BigDecimal bigDecimal = new BigDecimal(result);
        System.out.println(bigDecimal.setScale(1, RoundingMode.DOWN));
        return result;
    }

    public static void main(String[] args) {
        Triangle.getSquare(4, 5, 45);

    }
    // END
}
