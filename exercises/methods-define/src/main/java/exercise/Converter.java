package exercise;

class Converter {
    // BEGIN
    public static int convert(int amount, String type) {
        double value = switch (type) {
            case "b" -> 1024;
            case "kb" -> 0.0009765625;
            default -> 0;
        };

        return (int) (amount * value);

    }

    public static void main(String[] args) {
        int value = 10;
        String typeByte = "b";
        String typeKbyte = "Kb";
        int conversionResult = convert(value, typeByte);
        String output = String.format("%d %s = %d %s", value, typeKbyte, conversionResult, typeByte);
        System.out.println(output);
    }
    // END
}
