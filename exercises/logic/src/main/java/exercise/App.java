package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable;
        if (number % 2 != 0 && number >= 1001) {
            isBigOddVariable = true;
        } else isBigOddVariable = false;
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        String result = number % 2 == 0 ? "yes" : "no";
        System.out.println(result);
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        switch ((int) minutes / 15) {
            case 0:
                System.out.println("First");
                break;
            case 1:
                System.out.println("Second");
                break;
            case 2:
                System.out.println("Third");
                break;
            default:
                System.out.println("Fourth");
        }
        // END
    }
}
