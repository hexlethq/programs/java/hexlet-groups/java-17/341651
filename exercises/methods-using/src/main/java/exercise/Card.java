package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        StringBuilder stringBuilder = new StringBuilder(cardNumber);
        String start = cardNumber.substring(0, starsCount);

        StringBuilder stringBuilder2 = new StringBuilder(start);
        if (starsCount == 3) {
            stringBuilder2.replace(0, starsCount, "***");
        } else if (starsCount == 4) {
            stringBuilder2.replace(0, starsCount, "****");
        } else {
            stringBuilder2.replace(0, starsCount, "**");
        }
        String lastNumber = stringBuilder.substring(12);

        System.out.println(stringBuilder2 + lastNumber);
        // END
    }
}
