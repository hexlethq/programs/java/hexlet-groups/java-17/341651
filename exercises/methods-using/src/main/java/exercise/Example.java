class Example {

    public static void main(String[] args) {

        // Calling methods

        String sentence = "Is it question?";
        boolean isQuestion = sentence.endsWith("?");
        System.out.println(isQuestion); // true

        String newString = sentence.substring(6);
        System.out.println(newString); // "question?"

        // Methods without arguments

        String word = "Java";
        int length = word.length();
        System.out.println(length); // 4

        // Variables in arguments

        // Change all spaces to commas
        char space = ' ';
        char comma = ',';
        String sentence1 = "one two three";
        String result = sentence1.replace(space, comma);
        System.out.println(result); // "one,two,three"


        // Fills the string with spaces to the specified length (length = 5)
        String word1 = "abc";
        System.out.println(String.format("%5s", word1)); // "  abc"

        // Expression in arguments

        int newLength = 7;
        String filled = String.format("%" + newLength + "s", word1);
        System.out.println(filled); // "    abc"

        System.out.println(word.toLowerCase()); // "java"

        // Immutability of arguments

        String wordBeforeChange = "hello";
        String wordAfterChange = wordBeforeChange.toUpperCase();
        System.out.println(wordAfterChange); // "HELLO"
        System.out.println(wordBeforeChange); // "hello"
    }
}
